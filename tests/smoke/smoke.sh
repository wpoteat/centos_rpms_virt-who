#!/bin/bash


# takes in a command and makes sure that it returns success
# and that nothing is sent to stderr
function smoke {
  echo -n "Smoke test: '$@': "
  ERROR=$("$@" 2>&1> /dev/null)
  RETVAL=$?

  if [ -z "$ERROR" ] && [[ $RETVAL == 0 ]]; then
    echo "PASS"
  else
    echo "FAIL"
    echo "RETVAL: $RETVAL"
    echo "STDERR: $ERROR"
    exit 1
  fi
}

SMOKE_CMDS="virt-who --help"

while read -r CMD; do
  smoke $CMD
done <<<"$SMOKE_CMDS"
